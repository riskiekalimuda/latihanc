﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LatihanSoal.DELEGATE_TRIAL
{
    public delegate int DelegateHandler(int a, int b);
    public class ResultOperation
    {
        public int sum(int a, int b)
        {
            return a + b;
        }
        public void ProcessResultOperation()
        {
            ResultOperation result = new ResultOperation();
            DelegateHandler handler = new DelegateHandler(result.sum);
            int handlerResult = handler(50, 20);
            Console.WriteLine("Hasil penjumlahan: " + handlerResult);
            Console.ReadLine();
        }
    }
}
